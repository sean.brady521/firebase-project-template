import { Request } from 'express'

export interface VerifiedRequest extends Request {
  userId?: string
}