import { Response, Express, Request } from 'express'

export default (app: Express): void => {
  app.get ('/api/test', (req: Request, res: Response) => {
    res.status(200).send('beans')
  })
}
