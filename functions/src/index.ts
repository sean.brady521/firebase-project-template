const functions = require('firebase-functions') //eslint-disable-line

import express from 'express'

import admin from 'firebase-admin'

import cors from 'cors'

import testRoutes from './routes/test-routes'

const app = express()

app.use(cors({ origin: true }))
app.use(express.json())
app.use(express.urlencoded())

admin.initializeApp()

testRoutes(app)

exports.app = functions.https.onRequest(app)