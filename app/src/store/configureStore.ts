import { createStore, combineReducers, applyMiddleware, compose, Store } from 'redux'
import thunk from 'redux-thunk'
import { createBrowserHistory } from 'history'
import { connectRouter, routerMiddleware } from 'connected-react-router'

import userReducer from './user/user-reducers'

import { StoreState } from 'types/common'

declare global { interface Window { __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose } }

export const history = createBrowserHistory()

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const initialState = {}

const configureStore = (): Store => {
  const store = createStore(
    combineReducers<StoreState>({ router: connectRouter(history), user: userReducer }),
    initialState,
    composeEnhancers( applyMiddleware(thunk, routerMiddleware(history)))
  )

  return store
}

export default configureStore