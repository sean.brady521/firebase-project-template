import firebase from 'firebase/app'
import 'firebase/auth'
import { checkEmailSignIn } from 'utils/auth'
import { Dispatch } from 'redux'
import { replace } from 'connected-react-router'
import { SetErrorAction, SetLoadingAction, SetLoggedInAction } from './user-action-types'

export const setLoggedIn = (loggedIn: boolean): SetLoggedInAction => ({
  type: 'SET_LOGGED_IN',
  payload: { loggedIn }
})

export const setLoading = (loading: boolean): SetLoadingAction => ({
  type: 'SET_LOADING',
  payload: { loading }
})

export const setError = (error: string): SetErrorAction => ({
  type: 'SET_ERROR',
  payload: { error }
})

export const initAuthManager = (onLogin?: () => void) => {
  return async (dispatch: Dispatch): Promise<void> => {
    await checkEmailSignIn(() => { 
      dispatch(replace('/'))
    })
    firebase.auth().onAuthStateChanged(user => {
      dispatch(setLoggedIn(!!user))
      dispatch(setLoading(false))
      if (user && onLogin) onLogin()
    })
  }
}
