import React from 'react'
import { Route, Switch } from 'react-router-dom'

import TestPage from 'components/test'

export interface AppProps {
  loading: boolean
}

const App = (props: AppProps): JSX.Element => {
  const { loading } = props

  console.log(loading)
  
  return (
    <div>
      <Switch>
        <Route exact path='/' component={TestPage} />
      </Switch>
    </div>
  )
}
export default App
