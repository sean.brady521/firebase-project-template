import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { StoreState, } from 'types/common'
import App, { AppProps } from './app'
import { initAuthManager } from 'store/user/user-actions'
import { initialiseFirebase } from 'utils/auth'
import { Dispatch } from 'redux'

interface AppContainerProps extends AppProps {
  startAuthManager: () => void
}

export const AppContainer = (props: AppContainerProps): JSX.Element => {
  const { startAuthManager, ...rest } = props

  useEffect(() => {
    initialiseFirebase()
    startAuthManager()
  }, [])

  return <App {...rest} />
}

export function mapStateToProps(state: StoreState): AppProps {
  const { loading } = state.user
  return { loading }
}

export function mapDispatchToProps(dispatch: Dispatch): Pick<AppContainerProps, 'startAuthManager'> {
  const prefetchData = () => { 
    return {}
  }

  const startAuthManager = async () => {
    await initAuthManager(prefetchData)(dispatch)
  }

  return {
    startAuthManager
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer)