import React from 'react'
import styles from './test.module.scss'

interface TestProps {
  test?: string
}

const Test = (props: TestProps): JSX.Element => {
  const { test = 'test' } = props
  return ( 
    <div className={styles.root}>
      {test}
    </div>
  )
}

export default Test