import { RouterState } from 'connected-react-router'

export interface StoreState {
  router: RouterState
  user: UserState
}

export interface UserState {
  loading: boolean
  loggedIn: boolean
  error: string | null
}