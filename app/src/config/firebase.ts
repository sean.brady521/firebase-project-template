export const firebaseConf = { }

export const actionCodeSettings = {
  url: process.env.NODE_ENV === 'development' 
    ? 'http://localhost:3000' 
    : '',
  handleCodeInApp: true,
}